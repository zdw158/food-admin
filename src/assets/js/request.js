import Axios from 'axios'

/**
 * 创建请求对象
 */
const httpL = Axios.create({
  withCredentials: false,
  timeout: 1000 * 30
})

/**
 * 请求拦截
 */
httpL.interceptors.request.use(config => {
  config.headers['token'] = localStorage.getItem('token') // 请求头带上token
  return config
}, error => {
  return Promise.reject(error)
})

/**
 * 响应拦截
 */
httpL.interceptors.response.use(response => {
  if (response.data && response.data.code === 401) { // 401, token失效
    localStorage.removeItem('token') // 删除无效token
  }
  return response
}, error => {
  return Promise.reject(error)
})

const httpLRequest = ({url, type, data, headesType, file} = {}) => {
  return new Promise(function (resolve, reject) {
    httpL({
      url,
      method: type || 'POST',
      headers: {
        'Content-Type': headesType || (file ? '' : 'application/json; charset=utf-8')
      },
      data: file ? data : JSON.stringify(data)
    }).then(({data}) => {
      // console.log(res.data);
      if (data.code === '200') {
        resolve(data.data)
      } else {
        reject(data.msg)
      }
    }).catch(res => {
      reject(res)
    })
  })
}

export default httpLRequest
